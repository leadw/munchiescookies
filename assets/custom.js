$(document).ready(function() {
  ProductTabInit();
  SliderInit();
  MiniCart();
})

const ProductTabInit = function() {
  $(document).on('click', '.meta-info-header-item', function() {
    var handle = $(this).attr('data-handle');
    $('.meta-info-header-item').removeClass('active');
    $(this).addClass('active');

    $('.meta-info-content-item').removeClass('active');
    $('.meta-info-content-item[data-content="' + handle + '"]').addClass('active');
  })
}

const SliderInit = function() {
  var $slider = $('[data-mob-slider]');
  if($(window).width() < 750) {
    $slider.slick({
      arrows: true,
      dots: false,
      prevArrow: '<button type="button" class="slick-prev"><svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none"><path d="M15 6L9 12L15 18" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg></button>',
      nextArrow: '<button type="button" class="slick-next"><svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24" fill="none"><path d="M9 6L15 12L9 18" stroke="currentColor" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/></svg></button>'
    });
  }
}


const MiniCart = function() {

  var $cartWrapper = $('.cart-drawer-wrapper');
  var $cartDrawer = $('#drawer--is-open');
  var $cartProducts = $('[data-cart-products]');

  var cart_open = true;

  $(document).on('click', '[data-cart-open]', function(e){
    e.preventDefault();
    cart_open = true;
    cartOpen();
  })

  $(document).on('click', '[data-cart-close]', function(e) {
    e.preventDefault();
    cart_open = false;
    cartClose();
  })

  $(document).on('click', '.js-qty__adjust--minus', function(e) {
    e.preventDefault();
    var id = $(this).closest('.cart__item').attr('data-key');
    var qty = $(this).closest('.js-qty__wrapper').find('input').val();
    $(this).closest('[data-cart-products]').parent().addClass('qty-loading');
    updateQty(id, parseInt(qty) - 1);
  })

  $(document).on('click', '.js-qty__adjust--plus', function(e) {
    e.preventDefault();
    var id = $(this).closest('.cart__item').attr('data-key');
    var qty = $(this).closest('.js-qty__wrapper').find('input').val();
    $(this).closest('[data-cart-products]').parent().addClass('qty-loading');
    updateQty(id, parseInt(qty) + 1);
  })

  $(document).on('click', '.cart__remove', function(e) {
    e.preventDefault();
    var id = $(this).closest('.cart__item').attr('data-key');
    updateQty(id, 0);
  })

  function updateQty(id, qty) {
    var u_data = {};
    u_data[id] = qty;

    console.log(u_data);

    $.ajax({
      url: '/cart/update.js',
      type: 'post',
      dataType: 'json',
      data: {
        updates: u_data
      },
      success: function() {
        UpdateCart();
      }
    })
  }

  var $btn = null;
  $(document).on('click', '[data-add-to-cart-ajax]', function(e) {
    e.preventDefault();

    cart_open = true;

    $btn = $(this);
    $btn.addClass('btn--loading');
    var id = $(this).closest('form').find('[name="id"]').val();
    $.ajax({
      type: 'post',
      url: '/cart/add.js',
      dataType: 'json',
      data: {
        id: id,
        quantity: 1
      },
      success: function() {
        UpdateCart();
      }
    })
  })

  function UpdateCart() {
    
    $.ajax({
      type: 'GET',
      url: '/cart?view=ajax',
      success: function(res) {
        var $resultDom = $(res);

        var itemCount = parseInt($resultDom.attr('data-count'));
        var productHtml = $resultDom.html();

        var idx = 0;
        if(itemCount >= 6) {
          idx = 1;
        }
        if(itemCount >= 12) {
          idx = 2;
        }

        updateCartProgress(itemCount);

        $('[data-cart-count]').text(itemCount);
        if(itemCount > 0) {
          $('[data-cart-count]').addClass('cart-link__bubble--visible');
        }
        else {
          $('[data-cart-count]').removeClass('cart-link__bubble--visible');
        }
        $cartProducts.html(productHtml);
        $('[data-subtotal]').html($resultDom.attr('data-cart-subtotal'));

        if((itemCount > 4 && itemCount < 7) || (itemCount > 10 && itemCount < 13)) {
          
          $.ajax({
            url: '/cart/clear.js',
            type: 'post',
            dataType: 'json',
            success: function() {
              var data = [];
              $('[data-cart-products]').find('.cart__item').each(function() {
                var row_data = {};

                var ids = ($(this).attr('data-id')).split(',');
                var id = ids[idx];

                row_data['quantity'] = parseInt($(this).find('input.js-qty__num').val());
                row_data['id'] = id;
                data.push(row_data);
                data.reverse();
              })
              
              console.log(data);

              $.ajax({
                url: '/cart/add.js',
                type: 'post',
                dataType: 'json',
                data: {
                  items: data
                },
                success: function() {
                  if($btn != null) {
                    $btn.removeClass('btn--loading');
                  }
                  cartOpen();
                  $('.qty-loading').removeClass('qty-loading');
                }
              })
              updateProductID(idx);
            }

          });
        }
        else {
          if($btn != null) {
            $btn.removeClass('btn--loading');
          }
          cartOpen();
          $('.qty-loading').removeClass('qty-loading');
          updateProductID(idx);
        }
      }
    })

  }

  function updateProductID(idx) {
    $('form.data-ajax-form').each(function() {
      var $input = $(this).find('input[name="id"]');
      var id = ($input.attr('data-id')).split(',');
      $input.val(id[idx]);
    })

    var $select = $('select[name="id"].product-single__variants');
    $select.val($select.find('option').eq(idx).val());

  }

  function cartOpen() {
    if(cart_open) {
      $cartWrapper.show();
      setTimeout(function() {
        $cartWrapper.addClass('is-open').addClass('js-drawer-open');
      }, 10);
    }
  }

  function cartClose() {
    $cartWrapper.removeClass('is-open');
    setTimeout(function() {
      $cartWrapper.removeClass('js-drawer-open').hide();
    }, 450);
  }

  function updateCartProgress(count) {
    var _m = count % 6;
    var node1 = 6, node2 = 12;

    if(count > 6) {
      node1 = count - _m;
      node2 = node1 + 6;
    }

    var $checkout = $('button.cart__checkout');
    if(count > 0) {
      if(_m == 0) {
        $checkout.removeAttr('disabled');
      }
      else {
        $checkout.attr('disabled', true);
      }
    }
    else {
      $checkout.attr('disabled', true);
    }

    var $node1 = $('.node1');
    var $node2 = $('.node2');
    $node1.html(node1);
    $node2.html(node2);

    var $p1 = $('.progress1');
    var $p2 = $('.progress2');

    var $pv1 = $('.progress1 span');
    var $pv2 = $('.progress2 span');

    if(count >= node1) {
      $node1.addClass('active');
      $p1.addClass('active');
      if(count >= node2) {
        $node2.addClass('active');
        $p2.addClass('active');
      }
      else {
        $node2.removeClass('active');
        $p2.removeClass('active');
        $pv2.css('width', 100 / 6 * _m + '%');
      }
    }
    else {
      $node1.removeClass('active');
      $p1.removeClass('active');

      $pv1.css('width', 100 / 6 * _m  + '%');
    }

  }


}